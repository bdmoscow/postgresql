--Главный запрос
SELECT d_agreement.agreement_rk,
       d_agreement.target,
       d_clients.age,
       d_clients.socstatus_work_fl,
       d_clients.socstatus_pens_fl,
       d_clients.gender,
       d_clients.child_total,
       d_clients.dependants,
       ds.personal_income,
       ct.loan_num_total,
       cs.loan_num_closed
FROM d_clients
JOIN d_agreement ON d_clients.id = d_agreement.id_client
JOIN (SELECT *
      FROM (SELECT id_client, personal_income, row_number() OVER (PARTITION BY id_client) AS rw
            FROM d_salary
            ORDER BY id_client) AS t1
      WHERE t1.rw = 1) AS ds ON d_clients.id = ds.id_client
JOIN (SELECT id_client, count(id_loan) AS loan_num_total
      FROM d_loan
      GROUP BY id_client
      ORDER BY id_client) AS ct ON d_clients.id = ct.id_client
JOIN (SELECT id_client, sum(closed_fl) AS loan_num_closed
      FROM d_loan
      JOIN d_close_loan ON d_loan.id_loan = d_close_loan.id_loan
      GROUP BY id_client
      ORDER BY id_client) AS cs ON d_clients.id = cs.id_client
ORDER BY d_clients.id;

SELECT personal_income
FROM d_salary;

SELECT * FROM d_loan;

SELECT * FROM d_close_loan;

SELECT * FROM d_clients ORDER BY id;

SELECT id, socstatus_pens_fl, socstatus_work_fl FROM d_clients ORDER BY id;

SELECT * FROM d_agreement ORDER BY id_client;

SELECT *
FROM (SELECT id_client, personal_income, row_number() OVER (PARTITION BY id_client) AS rw
      FROM d_salary
      ORDER BY id_client) AS t1
WHERE t1.rw = 1;

--общее количетсво ссуд
SELECT id_client, count(id_loan)
FROM d_loan
GROUP BY id_client
ORDER BY id_client;

SELECT *
FROM d_loan
ORDER BY id_client;

SELECT *
FROM d_close_loan
ORDER BY id_loan;

SELECT *
FROM d_loan
JOIN d_close_loan ON d_loan.id_loan = d_close_loan.id_loan;

--количество погашенных ссуд
SELECT id_client, sum(closed_fl) as loan_num_closed
FROM d_loan
JOIN d_close_loan ON d_loan.id_loan = d_close_loan.id_loan
GROUP BY id_client
ORDER BY id_client;

SELECT id_client, count(id_loan) as loan_num_total
FROM d_loan
GROUP BY id_client
ORDER BY id_client;

ALTER USER postgres WITH PASSWORD 'postgres';

UPDATE d_clients
SET socstatus_work_fl = socstatus_work_fl-1
WHERE socstatus_work_fl = 1;

SELECT * FROM d_work;

SELECT * FROM d_pens;

SELECT * FROM d_work
FULL OUTER JOIN d_pens dp on d_work.id = dp.id;